#include <errno.h>
// #include <stdio.h>
// #include <unistd.h>

#define STM32F1 1

#include "libopencm3/stm32/rcc.h"
#include "libopencm3/stm32/gpio.h"
#include "libopencm3/stm32/usart.h"

#include "delay.h"
#include "i2c.h"
#include "Adafruit_MCP9808.h"
#include "Adafruit_LEDBackpack.h"
#include "printf/printf.h"

static I2C_Control i2c;			// I2C Control struct

static void usart_setup() {
    // For the peripheral to work, we need to enable bothe GPIO pin clock and
    // the peripheral clock
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_USART1);

    // For USART1 on the STM32F103 we need to select alternate function, and since
    // the line will be driven externally no need to set PULLUP etc ...
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
    
    // Configure USART1
    // Baud rate = 115200
    usart_set_baudrate(USART1, 115200);
    // Data = 8 bits
    usart_set_databits(USART1, 8);
    // No parity bit
    usart_set_parity(USART1, USART_PARITY_NONE);
    // One stop bit
    usart_set_stopbits(USART1, USART_STOPBITS_1);
    // Transmit only
    usart_set_mode(USART1, USART_MODE_TX);
    // No flow control
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
    // Enable the peripheral
    usart_enable(USART1);
}

static void clock_setup() {
    // Setup clock on STM32F108 (Blue Pill - as it has
    // an external 8Mhz clock (HSE))
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
 }

static void gpio_setup() {
    // Since our LED is on GPIO bank C, we need to enable
    // the peripheral clock to this GPIO bank in order to use it.
    rcc_periph_clock_enable(RCC_GPIOC);

    // Our test LED is connected to Port C pin 13, so let's set it as output
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
}

void _putchar(char character) {
  // send char to console etc.
  usart_send_blocking(USART1, character);
}

void temp_monitor(void) {
    Adafruit_MCP9808 mcp = Adafruit_MCP9808();
    Adafruit_7segment matrix = Adafruit_7segment();

    if( !mcp.begin(MCP9808_I2CADDR_DEFAULT, &i2c) ) {
        printf("Unable to initialise MCP9808\r\n");
        while(1);
    }
    mcp.setResolution(MCP9808_REG_RESOLUTION_SIXTEENTH);

    matrix.begin(&i2c);
    matrix.setBrightness(0x10);
    matrix.clear();

    bool displayC = false;

    while(1) {
        uint64_t temp_s = millis();
        gpio_toggle(GPIOC, GPIO13);
        
        double tempvalueC = mcp.readTempC();
        double tempvalueF = tempvalueC * 1.8 + 32.0;

        printf("[%lld] Temp: %.4fC, %.4fF\r\n", temp_s, tempvalueC, tempvalueF);
        matrix.print((displayC = !displayC) ? tempvalueC : tempvalueF, 3);
        matrix.writeDisplay();

        uint64_t temp_e = millis();
        delay(5000 - (temp_e - temp_s));
    }
}

int main() {
    clock_setup();
    usart_setup();
    systick_setup();
    gpio_setup();
    i2c_setup(&i2c);

    temp_monitor();

    return 0;
}
